#run ./script.sh to build

#build for linux on windows
env GOOS=linux go build -o build/signalz

#windows build
go build -o build/signalz-win

#deploy to now and alias
cd build && now && now alias
