package main

import (
	"context"
	"errors"
	"time"
)

func (r *Resolver) AddSignalz(ctx context.Context, args struct {
	TICKER   string
	SIGNAL   string
	AREA     string
	TARGETS  string
	EXCHANGE string
	PROFILE  string
}) (*Signalz, error) {

	for _, user := range users {
		if user.id == 1 {
			nextID := int32(len(signalz) + 1)
			newSig := &Signalz{nextID, args.TICKER, args.SIGNAL, args.AREA, args.TARGETS, args.EXCHANGE, args.PROFILE}
			signalz = append(signalz, newSig)

			// create message event
			// subscription 1 ==> next
			go func() {
				select {
				case r.newSignalzEvent <- newSig: // sub2==>send event to resolver, resolv.go
				case <-time.After(1 * time.Second):
				}
			}()
			return newSig, nil
		}
	}
	return nil, errors.New("Cannot find user")

}
