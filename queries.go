package main

import (
	"context"
	"errors"
)

func (r *Resolver) GetSignals(ctx context.Context, args struct {
	LASTSIGNALID int32
}) (*[]*Signalz, error) {
	if args.LASTSIGNALID == -1 {
		return &signalz, nil
	}

	return nil, errors.New("No Signal")
}

func (r *Resolver) GetClosedSignals(ctx context.Context, args struct {
	LASTSIGNALID int32
}) (*[]*ClosedSignalz, error) {
	if args.LASTSIGNALID == -1 {
		return &closedSignalz, nil
	}

	return nil, errors.New("No Signal")
}
