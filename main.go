package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/rs/cors"

	"github.com/joho/godotenv"

	"github.com/gorilla/mux"
	graphql "github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"
	"github.com/graph-gophers/graphql-transport-ws/graphqlws"
)

func init() {
	// loads values from .env into the system
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}
}

// consider https://github.com/moovweb/gvm

func main() {

	s, err := getSchema("./schema.graphql")
	if err != nil {
		panic(err)
	}

	schema, err := graphql.ParseSchema(s, newResolver())
	if err != nil {
		panic(err)
	}

	graphQLHandler := graphqlws.NewHandlerFunc(schema, &relay.Handler{Schema: schema})

	// https://stackoverflow.com/questions/50282541/catch-all-url-in-golang/50282841#50282841
	r := mux.NewRouter()
	r.HandleFunc("/graphql", graphQLHandler)
	r.HandleFunc("/", YourHandler)
	r.NotFoundHandler = http.HandlerFunc(YourHandler)

	handler := cors.Default().Handler(r) //https://github.com/rs/cors/blob/master/examples/gorilla/server.go
	port := os.Getenv("PORT")
	env := os.Getenv("ENV")
	var url string
	if port == "" {
		if env == "dev" {
			port = "4000"
		} else {
			port = "8080"
		}
	}

	if env == "dev" {
		url = "localhost:" + port
	} else {
		url = "0.0.0.0:" + port
	}
	fmt.Println("App running @ http://" + url)
	log.Fatal(http.ListenAndServe(url, handler))
}
