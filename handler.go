package main

import (
	"net/http"
)

func YourHandler(w http.ResponseWriter, r *http.Request) {
	// https://stackoverflow.com/questions/50282541/catch-all-url-in-golang
	// route "/" catches all routes
	if r.URL.Path != "/" {
		// http.NotFound(w, r)
		w.Write([]byte(r.URL.Path + " " + "is not found on this server. Consider using the graphql endpoint @ /graphql"))
		return
	}
	w.Write([]byte("Signalz api. Access the graphql endpoint at /graphql\n"))
}
