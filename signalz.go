package main

import (
	"context"
)

type Signalz struct {
	id       int32
	ticker   string
	signal   string
	area     string
	targets  string
	exchange string
	profile  string
}

func (u *Signalz) ID(ctx context.Context) int32 {
	return u.id
}

func (u *Signalz) TICKER(ctx context.Context) string {
	return u.ticker
}

func (u *Signalz) SIGNAL(ctx context.Context) string {
	return u.signal
}

func (u *Signalz) AREA(ctx context.Context) string {
	return u.area
}

func (u *Signalz) TARGETS(ctx context.Context) string {
	return u.targets
}

func (u *Signalz) EXCHANGE(ctx context.Context) string {
	return u.exchange
}

func (u *Signalz) PROFILE(ctx context.Context) string {
	return u.profile
}

type ClosedSignalz struct {
	id      int32
	ticker  string
	signal  string
	area    string
	targets string
	gains   string
	profile string
}

func (u *ClosedSignalz) ID(ctx context.Context) int32 {
	return u.id
}

func (u *ClosedSignalz) TICKER(ctx context.Context) string {
	return u.ticker
}

func (u *ClosedSignalz) SIGNAL(ctx context.Context) string {
	return u.signal
}

func (u *ClosedSignalz) AREA(ctx context.Context) string {
	return u.area
}

func (u *ClosedSignalz) TARGETS(ctx context.Context) string {
	return u.targets
}

func (u *ClosedSignalz) GAINS(ctx context.Context) string {
	return u.gains
}

func (u *ClosedSignalz) PROFILE(ctx context.Context) string {
	return u.profile
}

// func (u *User) PASSWORD(ctx context.Context) string {
// 	return u.password
// }

var signalz = []*Signalz{
	&Signalz{1, "BTC/STEEM", "Buy", "1200 - 1400", "2400 - 3000", "Bittrex", "Alltargetcrossed"},
	&Signalz{2, "BTC/ETH", "Buy", "1500 - 1700", "5400 - 6000", "Poloniex", "Alltargetcrossed"},
}

var closedSignalz = []*ClosedSignalz{
	&ClosedSignalz{1, "BTC/STEEM", "Sell", "1200 - 1400", "2400 - 3000", "10%", "Alltargetcrossed"},
	&ClosedSignalz{2, "BTC/ETH", "Buy", "1500 - 1700", "2400 - 3000", "50%", "Alltargetcrossed"},
}
