package main

import "context"

type User struct {
	id       int32
	username string
	password string
}

func (u *User) ID(ctx context.Context) int32 {
	return u.id
}

func (u *User) USERNAME(ctx context.Context) string {
	return u.username
}

// func (u *User) PASSWORD(ctx context.Context) string {
// 	return u.password
// }

var users = []*User{
	&User{1, "eltneg", "pass"},
	&User{2, "eltneg1", "pass1"},
}
