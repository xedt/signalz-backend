package main

import (
	"context"
)

// events and subscribers

type newSignalzSubscriber struct {
	stop   <-chan struct{} //recieve struct from channel
	events chan<- *Signalz // sub6 ==>send newMessageEvent into channel & user receives event last
}

// NewMsg creates new subscriber, entry point
func (r *Resolver) NewSignalz(ctx context.Context) <-chan *Signalz {
	c := make(chan *Signalz)
	// NOTE: this could take a while
	r.newSignalzSubscriber <- &newSignalzSubscriber{events: c, stop: ctx.Done()}

	return c
}
