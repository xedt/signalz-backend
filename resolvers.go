package main

import "time"

type Resolver struct {
	newSignalzEvent      chan *Signalz
	newSignalzSubscriber chan *newSignalzSubscriber
}

func newResolver() *Resolver {
	r := &Resolver{
		newSignalzEvent:      make(chan *Signalz),
		newSignalzSubscriber: make(chan *newSignalzSubscriber),
	}
	go r.broadcastSignalz()
	return r
}

func (r *Resolver) broadcastSignalz() {
	//a map{string: subscriber} of subscribers
	subscribers := map[string]*newSignalzSubscriber{}
	unsubscribe := make(chan string)
	// fmt.Println("Broadcast created---1")

	// NOTE: subscribing and sending events are at odds.
	for {
		select {
		case id := <-unsubscribe:
			// fmt.Println("UnSubscribing -----2")
			delete(subscribers, id)
		case s := <-r.newSignalzSubscriber:
			// fmt.Println("New subscriber -----3")
			subscribers[randomID()] = s
			// *****Update subscribers list and send
		case e := <-r.newSignalzEvent:
			// fmt.Println("New message -------4")
			for id, s := range subscribers {
				go func(id string, s *newSignalzSubscriber) {
					select {
					case <-s.stop:
						// fmt.Println("case ----1")
						unsubscribe <- id
						// *****Update subscribers list and send
						return
					default:
						// fmt.Println("default ----2")
					}

					select {
					case <-s.stop:
						// fmt.Println("case ----3")
						unsubscribe <- id
						// *****Update subscribers list and send
					case s.events <- e: // sub5 ==>send event to this subscriber
						// fmt.Println("case ----4")
					case <-time.After(time.Second):
						// fmt.Println("case time out ----5")
					}
				}(id, s)
			}
		}
	}
}
