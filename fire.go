package main

import (
	"context"
	"log"

	firebase "firebase.google.com/go"
)

// opt := option.WithCredentialsFile()

func FirebaseApp() *firebase.App {
	conf := &firebase.Config{ServiceAccountID: "firebase-adminsdk-exgtt@signalz.iam.gserviceaccount.com"}
	app, err := firebase.NewApp(context.Background(), conf)
	if err != nil {
		log.Fatal("err")
	}
	return app
}
